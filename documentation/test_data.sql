-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Server version:               5.5.53-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumping data for table task_management_app.statuses: ~3 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`status_id`, `status_name`) VALUES
	(1, 'NOT_STARTED'),
	(2, 'IN_PROGRESS'),
	(3, 'FINISHED');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping data for table task_management_app.tasks: ~10 rows (approximately)
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` (`task_id`, `title`, `description`, `status_id`, `user_id`) VALUES
	(2, 'Documents', 'Prepare documents for work', 3, 1),
	(4, 'Spring Security', 'Implement JWT token', 1, 1),
	(5, 'Service layer', 'Check the code in the service layer', 1, 1),
	(7, 'Unit tests', 'Write unit tests for my new app', 1, 1),
	(8, 'Credit card', 'Repay credit card', 1, 1),
	(11, 'Database', 'Create database diagram', 1, 1),
	(12, 'Project requirements', 'Define project requirements for the client', 2, 1),
	(13, 'Home Page', 'Choose design and colours of the home page', 2, 1),
	(15, 'Repository layer', 'Check the code in the repository layer', 1, 2),
	(16, 'REST API', 'Create a REST API for our new project', 2, 2);
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;

-- Dumping data for table task_management_app.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email`) VALUES
	(1, 'Ivan', 'Ivanov', 'ivan', '$2a$12$NXb0ceKgMsuf0sf0cX7GLuv5u31vLG/5yU1KZr6mulGjK2sGa2Ywe', 'ivan@mail.com'),
	(2, 'Petar', 'Petrov', 'petar', '$2a$12$HoT0KPJA./YGiQOKrJpngOi2.IWQlA97cgxy3azWI4oYbompounzW', 'petar@mail.com'),
	(5, 'Kalina', 'Kalinova', 'kalina', '$2a$12$VQsZ0E93l0PeJjfZ7LPFxu3Hf9Olhb12iRBmRalA8dzsKAYsYWLSa', 'kalina@mail.com'),
	(6, 'Maria', 'Ivanova', 'maria', '$2a$12$wA3lN5zOM0Vn.O68rAIcPeL/ehayZIDd1HqGNYEXZo2jqYgF9O1xy', 'maria@mail.com'),
	(7, 'Nikolay', 'Nikolov', 'nikolay', '$2a$12$5iOiBgRn.Q7xKu4KpCGb.eRGT69vhXIZxfryjQOX/IunTjDJpqSaW', 'nikolay@mail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

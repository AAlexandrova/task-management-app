# Task Management App



## Purpose

Task Management App is a web application intended for busy professionals. The purpose of the app is to help people to organise and manage their tasks.

## User profile

In order to use the app, the user should sign up and create a user profile. 

## Tasks

Each task has a title, description and status. By default, the status of each newly created task is "Not started".
The user can create, update and delete tasks. The user can also change the status of a task or delegate the task to another user. A task with status "Finished" cannot be modified.

The user can view a list of his/her tasks, filter and sort them and export them to PDF.

## REST API

A REST API is also provided. Documentation can be found here:
https://app.swaggerhub.com/apis/ALBENAAALEXANDROVA/Task-management-app/v0


package services;

import com.company.taskmanagementapp.models.Status;
import com.company.taskmanagementapp.models.enums.StatusName;
import com.company.taskmanagementapp.repositories.contracts.StatusRepository;
import com.company.taskmanagementapp.services.StatusServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.taskmanagementapp.Helpers.createMockStatus;

@ExtendWith(MockitoExtension.class)
public class StatusServiceImplTests {
    @Mock
    StatusRepository mockStatusRepository;

    @InjectMocks
    StatusServiceImpl statusService;

    @Test
    public void getStatuses_Should_CallRepository() {
        // Act
        statusService.getStatuses();

        // Assert
        Mockito.verify(mockStatusRepository, Mockito.times(1)).
                getStatuses();
    }

    @Test
    public void getStatusByName_Should_CallRepository() {
        // Act
        statusService.getStatusByName(StatusName.NOT_STARTED);

        // Assert
        Mockito.verify(mockStatusRepository, Mockito.times(1))
                .getStatusByName(StatusName.NOT_STARTED);
    }

    @Test
    public void getStatusByName_Should_ReturnStatus_When_MatchByNameExists() {
        //Arrange
        Status mockRepositoryStatus = createMockStatus();
        Mockito.when(mockStatusRepository.getStatusByName(StatusName.NOT_STARTED))
                .thenReturn(mockRepositoryStatus);
        //Act
        Status status = statusService.getStatusByName(StatusName.NOT_STARTED);

        //Assert
        Assertions.assertEquals(mockRepositoryStatus, status);
    }
}

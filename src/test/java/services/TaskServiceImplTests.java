package services;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityDuplicateException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.models.Status;
import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.repositories.contracts.StatusRepository;
import com.company.taskmanagementapp.repositories.contracts.TaskRepository;
import com.company.taskmanagementapp.repositories.contracts.UserRepository;
import com.company.taskmanagementapp.services.TaskServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.taskmanagementapp.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class TaskServiceImplTests {

    @Mock
    TaskRepository mockTaskRepository;

    @Mock
    UserRepository mockUserRepository;

    @Mock
    StatusRepository mockStatusRepository;

    @InjectMocks
    TaskServiceImpl taskService;

    @Test
    void getTasksByUserId_Should_CallUserRepository() {
        // Arrange
        TaskFilterOptions filterOptions = createMockTaskFilterOptions();

        // Act
        taskService.getTasksByUserId(1, filterOptions);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getUserById(1);
    }

    @Test
    public void getTaskById_Should_CallRepository() {
        // Act
        taskService.getTaskById(1);
        // Assert
        Mockito.verify(mockTaskRepository, Mockito.times(1)).getTaskById(1);
    }

    @Test
    public void getTaskById_Should_ReturnTask_When_MatchByIdExists() {
        //Arrange
        Task mockRepositoryTask = createMockTask();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(mockRepositoryTask);
        //Act
        Task task = taskService.getTaskById(1);

        //Assert
        Assertions.assertEquals(mockRepositoryTask, task);
    }

    @Test
    public void getTaskById_Should_CallRepository_When_ValidAuthentication() {
        //Arrange
        Task task = createMockTask();
        User user = task.getUser();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);

        // Act
        taskService.getTaskById(1, user);
        // Assert
        Mockito.verify(mockTaskRepository, Mockito.times(1)).getTaskById(1);
    }

    @Test
    public void getTaskById_Should_ReturnTask_When_ValidAuthentication() {
        //Arrange
        User user = createMockUser();
        Task mockRepositoryTask = createMockTask();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(mockRepositoryTask);
        //Act
        Task task = taskService.getTaskById(1, user);

        //Assert
        Assertions.assertEquals(mockRepositoryTask, task);
    }

    @Test
    public void getTaskById_Should_ThrowException_When_InvalidAuthentication() {
        //Arrange
        User user = createMockUser();
        user.setUserId(2);
        user.setFirstName("newFirstName");
        user.setLastName("newLastName");

        Task task = createMockTask();
        Mockito.when(mockTaskRepository.getTaskById(1)).thenReturn(task);

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> taskService.getTaskById(1, user));
    }

    @Test
    public void createTask_Should_ThrowException_When_TaskWithSameTitleExists() {
        //Arrange
        Task mockRepositoryTask = createMockTask();
        Task newTask = createMockTask();
        newTask.setTaskId(2);
        newTask.setTitle("newTitle");
        newTask.setDescription("newDescription");
        Mockito.when(mockTaskRepository.getTaskByTitle(Mockito.any(), Mockito.anyString()))
                .thenReturn(mockRepositoryTask);

        //Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class,
                () -> taskService.createTask(newTask));
    }

    @Test
    public void createTask_Should_CallRepository_When_TaskWithSameTitleDoesNotExist() {
        //Arrange
        Task task = createMockTask();
        Mockito.when(mockTaskRepository.getTaskByTitle(Mockito.any(), Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        taskService.createTask(task);

        //Assert
        Mockito.verify(mockTaskRepository, Mockito.times(1)).createTask(task);
    }

    @Test
    public void updateTask_Should_ThrowException_When_InvalidAuthentication() {
        //Arrange
        Task task = createMockTask();
        User user = createMockUser();
        user.setUserId(2);
        user.setFirstName("newFirstName");
        user.setLastName("NewLastName");

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> taskService.updateTask(task, user));
    }

    @Test
    public void updateTask_Should_ThrowException_When_StatusFinished() {
        //Arrange
        Task task = createMockTask();
        task.setStatus(createMockStatusFinished());

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> taskService.updateTask(task, task.getUser()));
    }

    @Test
    public void updateTask_Should_ThrowException_When_TaskWithSameTitleExists() {
        //Arrange
        Task mockRepositoryTask = createMockTask();
        Task newTask = createMockTask();
        newTask.setTaskId(2);
        newTask.setTitle("newTitle");
        newTask.setDescription("newDescription");
        Mockito.when(mockTaskRepository.getTaskByTitle(Mockito.any(), Mockito.anyString()))
                .thenReturn(mockRepositoryTask);

        //Act, Assert
        Assertions.assertThrows(EntityDuplicateException.class,
                () -> taskService.updateTask(newTask, newTask.getUser()));
    }

    @Test
    public void updateTask_Should_CallRepository_When_TaskWithSameTitleDoesNotExist() {
        //Arrange
        Task task = createMockTask();
        Mockito.when(mockTaskRepository.getTaskByTitle(Mockito.any(), Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        taskService.updateTask(task, task.getUser());

        //Assert
        Mockito.verify(mockTaskRepository, Mockito.times(1)).updateTask(task);
    }

    @Test
    public void deleteTask_Should_ThrowException_When_InvalidAuthentication() {
        //Arrange
        Task task = createMockTask();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);

        User user = createMockUser();
        user.setUserId(2);
        user.setFirstName("newFirstName");
        user.setLastName("newLastName");

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> taskService.deleteTask(task.getTaskId(), user));
    }

    @Test
    public void deleteTask_Should_CallRepository_When_ValidAuthentication() {
        //Arrange
        Task task = createMockTask();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);
        // Act
        taskService.deleteTask(task.getTaskId(), task.getUser());
        // , Assert
        Mockito.verify(mockTaskRepository, Mockito.times(1)).deleteTask(task.getTaskId());
    }

    @Test
    public void changeStatus_Should_ThrowException_When_InvalidAuthentication() {
        //Arrange
        Task task = createMockTask();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);

        User user = createMockUser();
        user.setUserId(2);
        user.setFirstName("newFirstName");
        user.setLastName("newLastName");

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> taskService.changeStatus(task.getTaskId(), 1, user));
    }

    @Test
    public void changeStatus_Should_ThrowException_When_StatusFinished() {
        //Arrange
        Task task = createMockTask();
        task.setStatus(createMockStatusFinished());
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> taskService.changeStatus(task.getTaskId(), 1, task.getUser()));
    }

    @Test
    public void changeStatus_Should_CallRepository_When_ValidAuthentication() {
        // Arrange
        Task task = createMockTask();
        Status newStatus = createMockStatus();
        Mockito.when(mockTaskRepository.getTaskById(1)).thenReturn(task);
        Mockito.when(mockStatusRepository.getStatusById(1)).thenReturn(newStatus);

        // Act
        taskService.changeStatus(1, 1, task.getUser());

        // Assert
        Mockito.verify(mockTaskRepository, Mockito.times(1)).updateTask(task);
    }

    @Test
    public void delegateTask_Should_ThrowException_When_InvalidAuthentication() {
        //Arrange
        Task task = createMockTask();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);

        User user = createMockUser();
        user.setUserId(2);
        user.setFirstName("newFirstName");
        user.setLastName("newLastName");

        // Act, Assert
        Assertions.assertThrows(AuthorizationException.class,
                () -> taskService.delegateTask(task.getTaskId(), 1, user));
    }

    @Test
    public void delegateTask_Should_ThrowException_When_StatusFinished() {
        //Arrange
        Task task = createMockTask();
        task.setStatus(createMockStatusFinished());
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> taskService.delegateTask(task.getTaskId(), 2, task.getUser()));
    }

    @Test
    public void delegateTask_Should_ThrowException_When_TryingToDelegateToTheSameUser() {
        //Arrange
        Task task = createMockTask();
        User user = task.getUser();
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);
        Mockito.when(mockUserRepository.getUserById(Mockito.anyInt())).thenReturn(user);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> taskService.delegateTask(task.getTaskId(), 1, user));
    }

    @Test
    public void delegateTask_Should_CallRepository_When_ValidAuthentication() {
        // Arrange
        Task task = createMockTask();
        User user = createMockUser();
        user.setUserId(2);
        user.setFirstName("newFirstName");
        user.setLastName("newLastName");
        Mockito.when(mockTaskRepository.getTaskById(Mockito.anyInt())).thenReturn(task);
        Mockito.when(mockUserRepository.getUserById(Mockito.anyInt())).thenReturn(user);

        // Act
        taskService.delegateTask(task.getTaskId(), user.getUserId(), task.getUser());

        // Assert
        Mockito.verify(mockTaskRepository, Mockito.times(1)).updateTask(task);
    }
}


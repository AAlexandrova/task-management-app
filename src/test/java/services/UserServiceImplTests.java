package services;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityDuplicateException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.repositories.contracts.UserRepository;
import com.company.taskmanagementapp.services.UserServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.company.taskmanagementapp.Helpers.createMockUser;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getUsers_Should_CallRepository() {
        // Act
        userService.getUsers();

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).
                getUsers();
    }

    @Test
    public void getUserByIdWithoutAuthentication_Should_CallRepository() {
        // Act
        userService.getUserById(1);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).getUserById(1);
    }

    @Test
    public void getUserByIdWithoutAuthentication_Should_ReturnUser_When_MatchByIdExist() {
        // Arrange
        User mockUser = createMockUser();
        when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserById(1);

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getUserById_Should_CallRepository() {
        //Arrange
        User mockUser = createMockUser();
        Mockito.when(mockUserRepository.getUserById(Mockito.anyInt()))
                .thenReturn(mockUser);

        // Act
        userService.getUserById(1, mockUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).getUserById(1);
    }

    @Test
    public void getUserById_Should_ThrowException_When_InvalidAuthentication() {
        //Arrange
        User user = createMockUser();
        Mockito.when(mockUserRepository.getUserById(1)).thenReturn(user);

        User authentication = createMockUser();
        authentication.setUserId(2);
        authentication.setFirstName("newFirstName");
        authentication.setLastName("newLastName");

        // Act, Assert
        Assertions.assertThrows(
                AuthorizationException.class,
                () -> userService.getUserById(1, authentication));
    }

    @Test
    public void getUserById_Should_ReturnUser_When_ValidAuthentication() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(mockUserRepository.getUserById(1)).thenReturn(mockUser);

        User authentication = createMockUser();

        // Act
        User result = userService.getUserById(1, authentication);

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void getUserByUsername_Should_CallRepository() {
        // Act
        userService.getUserByUsername(Mockito.anyString());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .getUserByUsername(Mockito.anyString());
    }

    @Test
    public void getUserByUsername_Should_ReturnUser_When_MatchByUsernameExist() {
        // Arrange
        User mockUser = createMockUser();
        Mockito.when(mockUserRepository.getUserByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        // Act
        User result = userService.getUserByUsername(mockUser.getUsername());

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void createUser_Should_CallRepository_When_UserWithSameNameAndEmailDoesNotExist() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        // Act
        userService.createUser(mockUser);

        // Assert
        verify(mockUserRepository, times(1))
                .createUser(mockUser);
    }

    @Test
    public void createUser_Should_ThrowException_When_UsernameIsTaken() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.createUser(mockUser));
    }

    @Test
    public void createUser_Should_ThrowException_When_EmailIsTaken() {
        // Arrange
        User mockUser = createMockUser();

        Mockito.when(mockUserRepository.getUserByUsername(mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockUserRepository.getUserByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        // Act, Assert
        Assertions.assertThrows(
                EntityDuplicateException.class,
                () -> userService.createUser(mockUser));
    }
}

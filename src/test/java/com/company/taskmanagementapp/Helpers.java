package com.company.taskmanagementapp;

import com.company.taskmanagementapp.models.Status;
import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.models.enums.StatusName;

public class Helpers {

    public static User createMockUser(){
        User mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("mockUser@mail.com");
        mockUser.setUsername("MockUsername");
        mockUser.setPassword("MockPassword111.");
        return mockUser;
    }

    public static Status createMockStatus(){
        Status mockStatus = new Status();
        mockStatus.setStatusId(1);
        mockStatus.setName(StatusName.NOT_STARTED);
        return mockStatus;
    }

    public static Status createMockStatusFinished(){
        Status mockStatus = new Status();
        mockStatus.setStatusId(3);
        mockStatus.setName(StatusName.FINISHED);
        return mockStatus;
    }

    public static Task createMockTask(){
        Task mockTask = new Task();
        mockTask.setTaskId(1);
        mockTask.setTitle("MockTitle");
        mockTask.setDescription("MockTaskDescription");
        mockTask.setUser(createMockUser());
        mockTask.setStatus(createMockStatus());
        return mockTask;
    }

    public static TaskFilterOptions createMockTaskFilterOptions(){
        return new TaskFilterOptions(
                "MockTitle",
                "MockTaskDescription",
                1,
                "sort",
                "sortOrder"
        );
    }
}

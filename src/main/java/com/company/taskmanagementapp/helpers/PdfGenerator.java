package com.company.taskmanagementapp.helpers;

import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.User;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.stereotype.Component;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class PdfGenerator {

    private static final Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.NORMAL, BaseColor.RED);
    private static final Font smallRedFont = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.NORMAL, BaseColor.RED);
    private static final Font tableFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL, BaseColor.BLACK);
    private static final Font tableHeading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.RED);

    public void createPdf(User user, List<Task> tasks, HttpServletResponse response)
            throws IOException, DocumentException {

        Document document = new Document();
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Paragraph preface = new Paragraph();
        preface.add(Chunk.NEWLINE);

        preface.add(new Paragraph(String.format("Hello, %s!", user.getFirstName()), redFont));
        preface.add(new Paragraph("These are your current tasks:", smallRedFont));
        preface.add(Chunk.NEWLINE);
     
        preface.add(Chunk.NEWLINE);
        PdfPTable tasksTable = generateTasksTable(user, tasks);
        preface.add(tasksTable);

        preface.add(Chunk.NEWLINE);

        document.add(preface);
        document.close();
    }

    private PdfPTable generateTasksTable(User user, List<Task> tasks) {
        PdfPTable table = new PdfPTable(3);

        PdfPCell c1 = new PdfPCell(new Phrase("Title", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Description", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Status", tableHeading));
        c1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table.addCell(c1);

        table.setHeaderRows(1);

        for (Task t : tasks) {
            table.addCell(new Phrase(t.getTitle(), tableFont));
            table.addCell(new Phrase(t.getDescription(), tableFont));
            table.addCell(new Phrase(t.getStatus().getName().toString(), tableFont));
        }

        return table;
    }
}

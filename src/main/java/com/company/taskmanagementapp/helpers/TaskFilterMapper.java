package com.company.taskmanagementapp.helpers;

import com.company.taskmanagementapp.models.dtos.TaskFilterDto;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TaskFilterMapper {
    public TaskFilterOptions dtoToObject(TaskFilterDto filterDto){
        TaskFilterOptions filterOptions = new TaskFilterOptions();

        if (filterDto.getTitle() != null && filterDto.getTitle().isEmpty()) {
            filterOptions.setTitle(Optional.empty());
        } else {
            filterOptions.setTitle(Optional.ofNullable(filterDto.getTitle()));
        }

        if (filterDto.getDescription() != null && filterDto.getDescription().isEmpty()) {
            filterOptions.setDescription(Optional.empty());
        } else {
            filterOptions.setDescription(Optional.ofNullable(filterDto.getDescription()));
        }

        filterOptions.setStatusId(Optional.ofNullable(filterDto.getStatusId()));
        filterOptions.setSortBy(Optional.ofNullable(filterDto.getSortBy()));
        filterOptions.setSortOrder(Optional.ofNullable(filterDto.getSortOrder()));

        return filterOptions;
    }
}

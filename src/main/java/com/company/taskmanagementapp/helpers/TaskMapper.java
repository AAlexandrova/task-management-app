package com.company.taskmanagementapp.helpers;

import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.models.dtos.TaskDtoIn;
import com.company.taskmanagementapp.models.enums.StatusName;
import com.company.taskmanagementapp.services.contracts.StatusService;
import com.company.taskmanagementapp.services.contracts.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TaskMapper {

    private final TaskService taskService;
    private final StatusService statusService;

    @Autowired
    public TaskMapper(TaskService taskService, StatusService statusService) {
        this.taskService = taskService;
        this.statusService = statusService;
    }

    public Task dtoToObject(TaskDtoIn taskDtoIn, User user){
        Task task = new Task();
        task.setTitle(taskDtoIn.getTitle());
        task.setDescription(taskDtoIn.getDescription());
        task.setUser(user);
        task.setStatus(statusService.getStatusByName(StatusName.NOT_STARTED));
        return task;
    }

    public Task dtoToObject(int taskId, TaskDtoIn taskDtoIn) {
        Task repositoryTask = taskService.getTaskById(taskId);
        Task task = dtoToObject(taskDtoIn, repositoryTask.getUser());
        task.setTaskId(taskId);
        task.setStatus(repositoryTask.getStatus());
        return task;
    }

    public TaskDtoIn objectToDto(Task task){
        TaskDtoIn taskDtoIn = new TaskDtoIn();
        taskDtoIn.setTitle(task.getTitle());
        taskDtoIn.setDescription(task.getDescription());
        return taskDtoIn;
    }
}

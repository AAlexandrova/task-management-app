package com.company.taskmanagementapp.helpers;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;

@Controller
public class AuthenticationHelper {
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String INVALID_AUTHENTICATION_MESSAGE = "Invalid authentication";
    private static final String INVALID_AUTHENTICATION_ERROR = "Invalid username and/or password";
    public static final String NO_LOGGED_USER_MESSAGE = "No user logged in.";
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationHelper(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_MESSAGE);
        }

        String userInfo = headers.getFirst(AUTHORIZATION_HEADER_NAME);
        String username = getUsername(userInfo);
        String password = getPassword(userInfo);
        return verifyAuthentication(username, password);
    }

    public User tryGetCurrentUser(HttpSession session) {
        String currentUsername = (String) session.getAttribute("currentUser");
        if (currentUsername == null) {
            throw new AuthorizationException(NO_LOGGED_USER_MESSAGE);
        }
        return userService.getUserByUsername(currentUsername);
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getUserByUsername(username);
            if (!passwordEncoder.matches(password, user.getPassword())) {
                throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_ERROR);
        }
    }

    private String getUsername(String userInfo) {
        int spaceIndex = userInfo.indexOf(" ");
        if (spaceIndex == -1) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_MESSAGE);
        }

        return userInfo.substring(0, spaceIndex);
    }

    private String getPassword(String userInfo) {
        int spaceIndex = userInfo.indexOf(" ");
        if (spaceIndex == -1) {
            throw new AuthorizationException(INVALID_AUTHENTICATION_MESSAGE);
        }

        return userInfo.substring(spaceIndex + 1);
    }
}


package com.company.taskmanagementapp.services;

import com.company.taskmanagementapp.services.contracts.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendDelegatedTaskEmail(String email, String username) {
        String subject = "You have been delegated a new task";
        String text = "Dear " + username + ",\n\n" +
                "You have been delegated a new task.\n" +
                "Please log in to your account to see the task.\n\n" +
                "Thank you for using our service!\n\n" +
                "Kind regards,\n" +
                "TaskManagementApp Team";
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setFrom("office.TaskManagementApp@gmail.com");
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }
}

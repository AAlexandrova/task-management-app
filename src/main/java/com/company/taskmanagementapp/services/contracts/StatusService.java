package com.company.taskmanagementapp.services.contracts;

import com.company.taskmanagementapp.models.Status;
import com.company.taskmanagementapp.models.enums.StatusName;

import java.util.List;

public interface StatusService {

    List<Status> getStatuses();

    Status getStatusByName(StatusName name);


}

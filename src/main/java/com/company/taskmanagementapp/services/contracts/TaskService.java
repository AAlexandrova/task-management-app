package com.company.taskmanagementapp.services.contracts;

import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import com.company.taskmanagementapp.models.User;

import java.util.List;

public interface TaskService {

    List<Task> getTasksByUserId(int userId, TaskFilterOptions filterOptions);

    Task getTaskById(int taskId);

    Task getTaskById(int taskId, User authentication);

    void createTask(Task task);

    void updateTask(Task task, User authentication);

    void deleteTask(int taskId, User authentication);

    void changeStatus(int taskId, int statusId, User authentication);

    void delegateTask(int taskId, int userId, User authentication);
}

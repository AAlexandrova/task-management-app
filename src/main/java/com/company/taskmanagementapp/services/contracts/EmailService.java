package com.company.taskmanagementapp.services.contracts;

public interface EmailService {
    void sendDelegatedTaskEmail(String email, String username);
}

package com.company.taskmanagementapp.services.contracts;

import com.company.taskmanagementapp.models.User;

import java.util.List;

public interface UserService {

    List<User> getUsers();
    User getUserById(int userId);

    User getUserById(int userId, User authentication);

    User getUserByUsername(String username);

    void createUser(User user);

}

package com.company.taskmanagementapp.services;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityDuplicateException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.models.*;
import com.company.taskmanagementapp.models.enums.StatusName;
import com.company.taskmanagementapp.repositories.contracts.StatusRepository;
import com.company.taskmanagementapp.repositories.contracts.TaskRepository;
import com.company.taskmanagementapp.repositories.contracts.UserRepository;
import com.company.taskmanagementapp.services.contracts.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {
    public static final String INCORRECT_DELEGATE_USER_ERROR = "You cannot delegate a task to yourself. Please choose another user.";
    public static final String ACCESS_DENIED_ERROR = "A user can view and modify only his/her own tasks.";
    public static final String FINISHED_STATUS_ERROR = "Finished tasks cannot be modified.";
    private final TaskRepository taskRepository;
    private final StatusRepository statusRepository;
    private final UserRepository userRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, StatusRepository statusRepository,
                           UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.statusRepository = statusRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Task> getTasksByUserId(int userId, TaskFilterOptions filterOptions) {
        User user = userRepository.getUserById(userId);
        return taskRepository.getTasks(user, filterOptions);
    }

    @Override
    public Task getTaskById(int taskId) {
        return taskRepository.getTaskById(taskId);
    }

    @Override
    public Task getTaskById(int taskId, User authentication) {
        Task task = taskRepository.getTaskById(taskId);
        checkAuthorization(task, authentication);
        return task;
    }

    @Override
    public void createTask(Task task) {
        boolean duplicateTitleExists = true;
        try {
            taskRepository.getTaskByTitle(task.getUser(), task.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateTitleExists = false;
        }
        if (duplicateTitleExists) {
            throw new EntityDuplicateException("Task", "title", task.getTitle());
        }
        taskRepository.createTask(task);
    }

    @Override
    public void updateTask(Task task, User authentication) {
        checkAuthorization(task, authentication);
        checkStatus(task);

        boolean duplicateTitleExists = true;
        try {
            Task existingTask = taskRepository.getTaskByTitle(task.getUser(), task.getTitle());
            if (existingTask.getTaskId() == task.getTaskId()) {
                duplicateTitleExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateTitleExists = false;
        }

        if (duplicateTitleExists) {
            throw new EntityDuplicateException("Task", "title", task.getTitle());
        }

        taskRepository.updateTask(task);
    }

    @Override
    public void deleteTask(int taskId, User authentication) {
        Task task = taskRepository.getTaskById(taskId);
        checkAuthorization(task, authentication);
        taskRepository.deleteTask(taskId);
    }

    @Override
    public void changeStatus(int taskId, int statusId, User authentication) {
        Task task = taskRepository.getTaskById(taskId);
        checkAuthorization(task, authentication);
        checkStatus(task);
        Status newStatus = statusRepository.getStatusById(statusId);
        task.setStatus(newStatus);
        taskRepository.updateTask(task);
    }

    @Override
    public void delegateTask(int taskId, int userId, User authentication) {
        Task task = taskRepository.getTaskById(taskId);
        checkAuthorization(task, authentication);
        checkStatus(task);
        User newUser = userRepository.getUserById(userId);
        if (newUser.equals(authentication)) {
            throw new IllegalArgumentException(INCORRECT_DELEGATE_USER_ERROR);
        }
        task.setUser(newUser);
        taskRepository.updateTask(task);
    }

    private void checkAuthorization(Task task, User authentication) {
        if (!task.getUser().equals(authentication)) {
            throw new AuthorizationException(ACCESS_DENIED_ERROR);
        }
    }

    private void checkStatus(Task task) {
        if (task.getStatus().getName().equals(StatusName.FINISHED)) {
            throw new IllegalArgumentException(FINISHED_STATUS_ERROR);
        }
    }
}

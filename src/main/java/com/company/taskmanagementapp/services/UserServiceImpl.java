package com.company.taskmanagementapp.services;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityDuplicateException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.repositories.contracts.UserRepository;
import com.company.taskmanagementapp.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    public static final String ACCESS_DENIED_ERROR = "A user can view only his/her own user details.";
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.getUsers();
    }

    @Override
    public User getUserById(int userId) {
        return userRepository.getUserById(userId);
    }

    @Override
    public User getUserById(int userId, User authentication) {
        User user = userRepository.getUserById(userId);
        checkAuthorization(user, authentication);
        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public void createUser(User user) {

        boolean duplicateUsernameExists = true;
        try {
            userRepository.getUserByUsername(user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }
        if (duplicateUsernameExists) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }

        boolean duplicateEmailExists = true;
        try {
            userRepository.getUserByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        if (duplicateEmailExists) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }
        userRepository.createUser(user);
    }

    private void checkAuthorization(User user, User authentication) {
        if (!user.equals(authentication)) {
            throw new AuthorizationException(ACCESS_DENIED_ERROR);
        }
    }
}

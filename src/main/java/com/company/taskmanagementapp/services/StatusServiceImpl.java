package com.company.taskmanagementapp.services;

import com.company.taskmanagementapp.models.Status;
import com.company.taskmanagementapp.models.enums.StatusName;
import com.company.taskmanagementapp.repositories.contracts.StatusRepository;
import com.company.taskmanagementapp.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {
    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getStatuses() {
        return statusRepository.getStatuses();
    }

    @Override
    public Status getStatusByName(StatusName name) {
        return statusRepository.getStatusByName(name);
    }
}

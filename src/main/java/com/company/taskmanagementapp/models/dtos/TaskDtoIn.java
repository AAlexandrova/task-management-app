package com.company.taskmanagementapp.models.dtos;

import javax.validation.constraints.Size;

public class TaskDtoIn {
    public static final int TASK_TITLE_MIN_LENGTH = 8;
    public static final int TASK_TITLE_MAX_LENGTH = 30;
    public static final int TASK_DESCRIPTION_MIN_LENGTH = 10;
    public static final int TASK_DESCRIPTION_MAX_LENGTH = 250;
    public static final String TITLE_LENGTH_ERROR = "Task title should be between 8 and 30 symbols";
    public static final String DESCRIPTION_LENGTH_ERROR = "Task description should be between 10 and 250 symbols";

    @Size(min = TASK_TITLE_MIN_LENGTH, max = TASK_TITLE_MAX_LENGTH,
            message = TITLE_LENGTH_ERROR)
    private String title;

    @Size(min = TASK_DESCRIPTION_MIN_LENGTH, max = TASK_DESCRIPTION_MAX_LENGTH,
            message = DESCRIPTION_LENGTH_ERROR)
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

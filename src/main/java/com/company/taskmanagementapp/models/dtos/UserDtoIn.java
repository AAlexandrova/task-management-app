package com.company.taskmanagementapp.models.dtos;

import com.company.taskmanagementapp.validations.constraints.Password;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.validation.constraints.Size;

public class UserDtoIn {

    public static final int USER_FIRST_NAME_MIN_LENGTH = 3;
    public static final int USER_FIRST_NAME_MAX_LENGTH = 30;
    public static final int USER_LAST_NAME_MIN_LENGTH = 3;
    public static final int USER_LAST_NAME_MAX_LENGTH = 30;
    public static final int USERNAME_MIN_LENGTH = 3;
    public static final int USERNAME_MAX_LENGTH = 30;
    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 30;
    public static final int EMAIL_MIN_LENGTH = 5;
    public static final int EMAIL_MAX_LENGTH = 40;
    public static final String FIRST_NAME_LENGTH_ERROR = "First name should be between 3 and 30 symbols";
    public static final String LAST_NAME_LENGTH_ERROR = "Last name should be between 3 and 30 symbols";
    public static final String USERNAME_LENGTH_ERROR = "Username should be between 3 and 30 symbols";
    public static final String PASSWORD_LENGTH_ERROR = "Password should be between 8 and 30 symbols";
    public static final String EMAIL_LENGTH_ERROR = "E-mail should be between 5 and 40 symbols";

    @Size(min = USER_FIRST_NAME_MIN_LENGTH, max = USER_FIRST_NAME_MAX_LENGTH,
            message = FIRST_NAME_LENGTH_ERROR)
    private String firstName;

    @Size(min = USER_LAST_NAME_MIN_LENGTH, max = USER_LAST_NAME_MAX_LENGTH,
            message = LAST_NAME_LENGTH_ERROR)
    private String lastName;

    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH,
            message = USERNAME_LENGTH_ERROR)
    private String username;

    @JsonIgnore
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH,
            message = PASSWORD_LENGTH_ERROR)
    @Password
    private String password;

    @Size(min = EMAIL_MIN_LENGTH, max = EMAIL_MAX_LENGTH,
            message = EMAIL_LENGTH_ERROR)
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package com.company.taskmanagementapp.models.dtos;

import javax.validation.constraints.NotEmpty;

public class LoginDto {
    public static final String EMPTY_USERNAME_ERROR = "Username cannot be empty";
    public static final String EMPTY_PASSWORD_ERROR = "Password cannot be empty";

    @NotEmpty(message = EMPTY_USERNAME_ERROR)
    private String username;

    @NotEmpty(message = EMPTY_PASSWORD_ERROR)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.company.taskmanagementapp.models;

import java.util.Optional;

public class TaskFilterOptions {
    private Optional<String> title;
    private Optional<String> description;
    private Optional<Integer> statusId;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public TaskFilterOptions() {
        this(null, null, null, null, null);
    }

    public TaskFilterOptions(String title, String description, Integer statusId, String sortBy,
                             String sortOrder) {
        this.title = Optional.ofNullable(title);
        this.description = Optional.ofNullable(description);
        this.statusId = Optional.ofNullable(statusId);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getTitle() {
        return title;
    }

    public void setTitle(Optional<String> title) {
        this.title = title;
    }

    public Optional<String> getDescription() {
        return description;
    }

    public void setDescription(Optional<String> description) {
        this.description = description;
    }

    public Optional<Integer> getStatusId() {
        return statusId;
    }

    public void setStatusId(Optional<Integer> statusId) {
        this.statusId = statusId;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public void setSortBy(Optional<String> sortBy) {
        this.sortBy = sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Optional<String> sortOrder) {
        this.sortOrder = sortOrder;
    }
}

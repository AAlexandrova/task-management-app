package com.company.taskmanagementapp.models.enums;

public enum StatusName {
    NOT_STARTED,
    IN_PROGRESS,
    FINISHED;

    @Override
    public String toString() {
        switch (this) {
            case NOT_STARTED:
                return "Not started";
            case IN_PROGRESS:
                return "In progress";
            case FINISHED:
                return "Finished";
            default:
                return "Unknown status";
        }
    }
}

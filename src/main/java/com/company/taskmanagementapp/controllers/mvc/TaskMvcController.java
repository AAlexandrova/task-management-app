package com.company.taskmanagementapp.controllers.mvc;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityDuplicateException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.helpers.AuthenticationHelper;
import com.company.taskmanagementapp.helpers.TaskFilterMapper;
import com.company.taskmanagementapp.helpers.TaskMapper;
import com.company.taskmanagementapp.helpers.PdfGenerator;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.dtos.TaskDtoIn;
import com.company.taskmanagementapp.models.dtos.TaskFilterDto;
import com.company.taskmanagementapp.services.contracts.EmailService;
import com.company.taskmanagementapp.services.contracts.StatusService;
import com.company.taskmanagementapp.services.contracts.TaskService;
import com.company.taskmanagementapp.services.contracts.UserService;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/tasks")
public class TaskMvcController {

    private final TaskService taskService;
    private final TaskMapper taskMapper;
    private final StatusService statusService;
    private final UserService userService;
    private final EmailService emailService;
    private final TaskFilterMapper taskFilterMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TaskMvcController(TaskService taskService, TaskMapper taskMapper,
                             StatusService statusService, UserService userService,
                             EmailService emailService, TaskFilterMapper taskFilterMapper,
                             AuthenticationHelper authenticationHelper) {
        this.taskService = taskService;
        this.taskMapper = taskMapper;
        this.statusService = statusService;
        this.userService = userService;
        this.emailService = emailService;
        this.taskFilterMapper = taskFilterMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @GetMapping
    public String getTasks(@ModelAttribute("filter") TaskFilterDto filterDto,
                           HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            TaskFilterOptions filterOptions = taskFilterMapper.dtoToObject(filterDto);
            List<Task> tasks = taskService.getTasksByUserId(user.getUserId(), filterOptions);
            model.addAttribute("user", user);
            model.addAttribute("tasks", tasks);
            model.addAttribute("statuses", statusService.getStatuses());
            model.addAttribute("filter", filterDto);
            return "TasksView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/pdf")
    public String generatePdf(HttpServletResponse response, HttpSession session)
            throws DocumentException, IOException {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        response.setContentType("application/pdf");
        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD:HH:MM:SS");
        String currentDateTime = dateFormat.format(new Date());
        String headerkey = "Content-Disposition";
        String headervalue = "attachment; filename=pdf_" + currentDateTime + ".pdf";
        response.setHeader(headerkey, headervalue);

        List<Task> tasks = taskService.getTasksByUserId(user.getUserId(), new TaskFilterOptions());
        PdfGenerator generator = new PdfGenerator();
        generator.createPdf(user, tasks, response);
        return "PdfSuccessView";
    }

    @GetMapping("/{taskId}")
    public String getSingleTask(@PathVariable int taskId,
                                HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Task task = taskService.getTaskById(taskId, user);
            model.addAttribute("user", user);
            model.addAttribute("task", task);
            return "TaskView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showNewTaskPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        model.addAttribute("task", new TaskDtoIn());
        return "TaskCreateView";
    }

    @PostMapping("/new")
    public String createTask(@Valid @ModelAttribute("task") TaskDtoIn taskDtoIn,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        if (bindingResult.hasErrors()) {
            return "TaskCreateView";
        }

        try {
            Task task = taskMapper.dtoToObject(taskDtoIn, user);
            taskService.createTask(task);
            return "redirect:/tasks";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("title", "duplicate_task", e.getMessage());
            return "TaskCreateView";
        }
    }

    @GetMapping("/{taskId}/update")
    public String showUpdateTaskPage(@PathVariable int taskId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        try {
            Task task = taskService.getTaskById(taskId, user);
            TaskDtoIn taskDto = taskMapper.objectToDto(task);
            model.addAttribute("taskId", taskId);
            model.addAttribute("task", taskDto);
            return "TaskUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @PostMapping("/{taskId}/update")
    public String updateTask(@PathVariable int taskId,
                             @Valid @ModelAttribute("task") TaskDtoIn taskDtoIn,
                             BindingResult bindingResult,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }

        if (bindingResult.hasErrors()) {
            return "TaskUpdateView";
        }

        try {
            Task task = taskMapper.dtoToObject(taskId, taskDtoIn);
            taskService.updateTask(task, user);
            return "redirect:/tasks/{taskId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("title", "duplicate_task", e.getMessage());
            return "TaskUpdateView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ErrorView";
        }
    }

    @GetMapping("/{taskId}/status")
    public String showChangeStatusPage(@PathVariable int taskId,
                                       HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Task task = taskService.getTaskById(taskId, user);
            model.addAttribute("statuses", statusService.getStatuses());
            model.addAttribute("task", task);
            return "TaskChangeStatusView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{taskId}/status")
    public String changeStatus(@PathVariable int taskId,
                               @RequestParam("statusId") int statusId,
                               HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            taskService.changeStatus(taskId, statusId, user);
            return "redirect:/tasks/{taskId}";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ErrorView";
        }
    }

    @GetMapping("/{taskId}/delete")
    public String showDeleteTaskPage(@PathVariable int taskId,
                                     HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Task task = taskService.getTaskById(taskId, user);
            model.addAttribute("task", task);
            return "TaskDeleteView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{taskId}/delete")
    public String deleteTask(@PathVariable int taskId,
                             HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            taskService.deleteTask(taskId, user);
            return "redirect:/tasks";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
    }

    @GetMapping("/{taskId}/delegate")
    public String showDelegateTaskPage(@PathVariable int taskId,
                                       HttpSession session, Model model) {
        User authentication;
        try {
            authentication = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            Task task = taskService.getTaskById(taskId, authentication);
            List<User> users = userService.getUsers();
            users.remove(task.getUser());
            model.addAttribute("users", users);
            model.addAttribute("task", task);
            return "TaskDelegateView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{taskId}/delegate")
    public String delegateTask(@PathVariable int taskId,
                               @RequestParam("userId") int userId,
                               HttpSession session, Model model) {
        User authentication;
        try {
            authentication = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            User newUser = userService.getUserById(userId);
            taskService.delegateTask(taskId, userId, authentication);
            emailService.sendDelegatedTaskEmail(newUser.getEmail(), newUser.getUsername());
            return "TaskDelegateSuccessView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "ErrorView";
        }
    }
}

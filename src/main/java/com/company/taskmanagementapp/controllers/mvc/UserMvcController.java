package com.company.taskmanagementapp.controllers.mvc;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityDuplicateException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.helpers.AuthenticationHelper;
import com.company.taskmanagementapp.helpers.UserMapper;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.models.dtos.UserDtoIn;
import com.company.taskmanagementapp.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    public UserMvcController(UserService userService, UserMapper userMapper
            , AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserId")
    public Integer populateCurrentUserId(HttpSession session) {
        return (Integer) session.getAttribute("currentUserId");
    }

    @GetMapping("/{userId}")
    public String showSingleUser(@PathVariable int userId, HttpSession session, Model model) {
        User authentication;
        try {
            authentication = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/authentication/login";
        }
        try {
            model.addAttribute("user", userService.getUserById(userId, authentication));
            return "UserView";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showCreateUserPage(Model model) {
        model.addAttribute("user", new UserDtoIn());
        return "UserCreateView";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") UserDtoIn userDtoIn,
                             BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "UserCreateView";
        }
        try {
            User user = userMapper.dtoToObject(userDtoIn);
            userService.createUser(user);
            return "redirect:/authentication/login";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "duplicate_user", e.getMessage());
            return "UserCreateView";
        }
    }
}

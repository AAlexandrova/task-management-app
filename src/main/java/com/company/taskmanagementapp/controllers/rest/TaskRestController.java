package com.company.taskmanagementapp.controllers.rest;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityDuplicateException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.helpers.AuthenticationHelper;
import com.company.taskmanagementapp.helpers.TaskMapper;
import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.models.dtos.TaskDtoIn;
import com.company.taskmanagementapp.services.contracts.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestController {
    private final AuthenticationHelper authenticationHelper;
    private final TaskService taskService;
    private final TaskMapper taskMapper;

    @Autowired
    public TaskRestController(AuthenticationHelper authenticationHelper,
                              TaskService taskService, TaskMapper taskMapper) {
        this.authenticationHelper = authenticationHelper;
        this.taskService = taskService;
        this.taskMapper = taskMapper;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @Operation(summary = "Get all tasks with filter")
    @GetMapping
    public List<Task> getTasks(@RequestHeader HttpHeaders headers,
                               @RequestParam(required = false) String title,
                               @RequestParam(required = false) String description,
                               @RequestParam(required = false) Integer statusId,
                               @RequestParam(required = false) String sortBy,
                               @RequestParam(required = false) String sortOrder) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            TaskFilterOptions filterOptions = new TaskFilterOptions(title, description, statusId,
                    sortBy, sortOrder);
            return taskService.getTasksByUserId(user.getUserId(), filterOptions);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Get task by id")
    @GetMapping("/{taskId}")
    public Task getTaskById(@RequestHeader HttpHeaders headers,
                            @PathVariable int taskId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return taskService.getTaskById(taskId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Create task")
    @PostMapping
    public Task createTask(@RequestHeader HttpHeaders headers,
                           @Valid @RequestBody TaskDtoIn taskDtoIn) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Task task = taskMapper.dtoToObject(taskDtoIn, user);
            taskService.createTask(task);
            return task;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Duplicate entry")
    @Operation(summary = "Update task")
    @PutMapping("/{taskId}")
    public Task updateTask(@RequestHeader HttpHeaders headers,
                           @PathVariable int taskId,
                           @Valid @RequestBody TaskDtoIn taskDtoIn) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Task task = taskMapper.dtoToObject(taskId, taskDtoIn);
            taskService.updateTask(task, user);
            return task;
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityDuplicateException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Finished status")
    @Operation(summary = "Change task status")
    @PutMapping("{taskId}/status")
    public Task changeStatus(@RequestHeader HttpHeaders headers,
                             @PathVariable int taskId,
                             @RequestParam int statusId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            taskService.changeStatus(taskId, statusId, user);
            return taskService.getTaskById(taskId);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @ApiResponse(responseCode = "409", description = "Finished status")
    @Operation(summary = "Delegate task")
    @PutMapping("{taskId}/delegate")
    public void delegateTask(@RequestHeader HttpHeaders headers,
                             @PathVariable int taskId,
                             @RequestParam int userId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            taskService.delegateTask(taskId, userId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Delete task")
    @DeleteMapping("/{taskId}")
    public void deleteTask(@RequestHeader HttpHeaders headers,
                           @PathVariable int taskId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            taskService.deleteTask(taskId, user);
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

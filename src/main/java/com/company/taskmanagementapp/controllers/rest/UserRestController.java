package com.company.taskmanagementapp.controllers.rest;

import com.company.taskmanagementapp.exceptions.AuthorizationException;
import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.helpers.AuthenticationHelper;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.services.contracts.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @ApiResponse(responseCode = "200", description = "Successful operation")
    @ApiResponse(responseCode = "401", description = "Unauthorized operation")
    @ApiResponse(responseCode = "404", description = "Resource not found")
    @Operation(summary = "Get user by id")
    @GetMapping("/{userId}")
    public User getUserById(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User authentication = authenticationHelper.tryGetUser(headers);
            return userService.getUserById(userId, authentication);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}

package com.company.taskmanagementapp.controllers.rest;

import com.company.taskmanagementapp.models.Status;
import com.company.taskmanagementapp.services.contracts.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/statuses")
public class StatusRestController {
    private final StatusService statusService;

    @Autowired
    public StatusRestController(StatusService statusService) {
        this.statusService = statusService;
    }

    @GetMapping
    public List<Status> getStatuses(){
        return statusService.getStatuses();
    }
}


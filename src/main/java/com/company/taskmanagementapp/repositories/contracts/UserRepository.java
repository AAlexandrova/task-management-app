package com.company.taskmanagementapp.repositories.contracts;


import com.company.taskmanagementapp.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getUsers();
    User getUserById(int userId);

    User getUserByUsername(String username);

    User getUserByEmail(String email);
    User createUser(User user);
}

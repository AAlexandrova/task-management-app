package com.company.taskmanagementapp.repositories.contracts;

import com.company.taskmanagementapp.models.Status;
import com.company.taskmanagementapp.models.enums.StatusName;

import java.util.List;

public interface StatusRepository {
    List<Status> getStatuses();

    Status getStatusById(int statusId);

    Status getStatusByName(StatusName name);
}

package com.company.taskmanagementapp.repositories.contracts;

import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import com.company.taskmanagementapp.models.User;

import java.util.List;

public interface TaskRepository {
    List<Task> getTasks(User user, TaskFilterOptions filterOptions);

    Task getTaskById(int taskId);

    Task getTaskByTitle(User user, String title);

    void createTask(Task task);

    void updateTask(Task task);

    void deleteTask(int taskId);
}

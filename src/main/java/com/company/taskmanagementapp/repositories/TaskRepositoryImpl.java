package com.company.taskmanagementapp.repositories;

import com.company.taskmanagementapp.exceptions.EntityNotFoundException;
import com.company.taskmanagementapp.models.Task;
import com.company.taskmanagementapp.models.TaskFilterOptions;
import com.company.taskmanagementapp.models.User;
import com.company.taskmanagementapp.repositories.contracts.TaskRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepositoryImpl implements TaskRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public TaskRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Task> getTasks(User user, TaskFilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Task t where t.user.userId = :userId ");
            Map<String, Object> queryParameters = new HashMap<>();
            List<String> queryParts = new ArrayList<>();

            filterOptions.getTitle().ifPresent(value -> {
                queryParts.add(" t.title like :title ");
                queryParameters.put("title", "%" + value + "%");
            });

            filterOptions.getDescription().ifPresent(value -> {
                queryParts.add("t.description like :description");
                queryParameters.put("description", "%" + value + "%");
            });

            filterOptions.getStatusId().ifPresent(value -> {
                queryParts.add("t.status.statusId = :statusId");
                queryParameters.put("statusId", value);
            });

            if (!queryParts.isEmpty()) {
                queryString.append(" and ").append(String.join(" and ", queryParts));
            }

            String sortString = generateSortString(filterOptions);
            queryString.append(sortString);

            Query<Task> query = session.createQuery(queryString.toString(), Task.class);
            query.setParameter("userId", user.getUserId());
            query.setProperties(queryParameters);
            return query.list();
        }
    }

    @Override
    public Task getTaskById(int taskId) {
        try (Session session = sessionFactory.openSession()) {
            Task task = session.get(Task.class, taskId);
            if (task == null) {
                throw new EntityNotFoundException("Task", taskId);
            }
            return task;
        }
    }

    @Override
    public Task getTaskByTitle(User user, String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Task> query = session
                    .createQuery("from Task t where t.user.userId = :userId and t.title = :title", Task.class);
            query.setParameter("userId", user.getUserId());
            query.setParameter("title", title);

            List<Task> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Task", "title", title);
            }
            return result.get(0);
        }
    }

    @Override
    public void createTask(Task task) {
        try (Session session = sessionFactory.openSession()) {
            session.save(task);
        }
    }

    @Override
    public void updateTask(Task task) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(task);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteTask(int taskId) {
        Task taskToDelete = getTaskById(taskId);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(taskToDelete);
            session.getTransaction().commit();
        }
    }

    private String generateSortString(TaskFilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        StringBuilder queryString = new StringBuilder(" order by ");
        switch (filterOptions.getSortBy().get()) {
            case "title":
                queryString.append(" t.title ");
                break;
            case "description":
                queryString.append(" t.description ");
                break;
            case "status":
                queryString.append(" t.status.name ");
                break;
            default:
                return "";
        }
        if (filterOptions.getSortOrder().isPresent() &&
                filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc ");
        }
        return queryString.toString();
    }
}


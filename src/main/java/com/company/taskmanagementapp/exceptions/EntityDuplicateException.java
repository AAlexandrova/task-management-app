package com.company.taskmanagementapp.exceptions;

public class EntityDuplicateException extends RuntimeException {

    public EntityDuplicateException(String item, String attribute, String value) {
        super(String.format("%s with %s '%s' already exists.", item, attribute, value));
    }
}
